// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function(){
    return gulp.src([
        'assets/scss/**/*.scss'
    ])
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(concat('site.min.css'))
    .pipe(prefix({
            browsers: ['last 2 versions', 'ie 11'],
            cascade: false
    }))
    .pipe(gulp.dest('html/assets/css'));
});

gulp.task('vendor-sass', function(){
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css'
    ])
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('html/assets/css'));
});

gulp.task('scripts', function(){
    return gulp.src([
        'assets/js/**/*.js'
    ])
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('html/assets/js'));
});

gulp.task('vendor-scripts', function(){
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.js'
    ])
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('html/assets/js'));
});

gulp.task('default', [ 'sass', 'scripts' ], function () {
  gulp.watch(['assets/scss/**/*.scss'], ['sass']);
  gulp.watch(['assets/js/**/*.js'], ['scripts']);
});
