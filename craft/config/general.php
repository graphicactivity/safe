<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
		'cpTrigger' => 'admin',
        'allowAutoUpdates' => false,
        'generateTransformsBeforePageLoad' => true,
    ),
    'safefromharm.dev' => array(
        'devMode' => true,
        'siteUrl' => '',
        'cooldownDuration' => 0,
        'env' => 'local',
    ),
    'safefromharm.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'http://safefromharm.co.nz',
        )
    )
);
